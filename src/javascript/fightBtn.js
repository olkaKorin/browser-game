import View from "./view";

class FightBtn extends View {
  constructor() {
    super();
    this.createFightBtn();
  }

  createFightBtn() {
    this.element = this.createElement({
      tagName: "button",
      className: "fight-btn"
    });
    this.element.innerText = "fight";
    this.element.addEventListener(
      "click",
      () => console.log("Let's fight! May the force be with you!"),
      false
    );
    return this.element;
  }
}

export default FightBtn;
