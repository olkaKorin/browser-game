import { getRandomNum } from "./helpers/utils";

class Fighter {
  constructor(attrs) {
    this.name = attrs.name;
    this._id = attrs._id;
    this.health = attrs.health;
    this.defense = attrs.defense;
    this.attack = attrs.attack;

    this.getHitPower = this.getHitPower.bind(this);
    this.getBlockPower = this.getBlockPower.bind(this);
    this.setHealth = this.setHealth.bind(this);
    this.setAttack = this.setAttack.bind(this);
  }

  getHitPower = () => {
    const criticalHitChance = getRandomNum(1, 2);
    return this.attack * criticalHitChance;
  };

  getBlockPower = () => {
    const dodgeChance = getRandomNum(1, 2);
    return this.defense * dodgeChance;
  };

  setHealth = hp => (this.health = hp);

  setAttack = attp => (this.attack = attp);
}

export default Fighter;
