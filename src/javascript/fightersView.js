import View from "./view";
import FighterView from "./fighterView";
import { fighterService } from "./services/fightersService";
import ModalView from "./modal-views/modalView";
import Fighter from "./fighter";
import FightBtn from "./fightBtn";
class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.updateFighter = this.updateFighter.bind(this);
    this.setFighterHealth = this.setFighterHealth.bind(this);
    this.setFighterAttack = this.setFighterAttack.bind(this);
    this.createFighters(fighters);
  }
  static rootElement = document.getElementById("root");

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const btnElement = new FightBtn().element;
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });
    this.element.append(btnElement, ...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    document.querySelectorAll(".modal").forEach(item => item.remove());

    try {
      const fighterDetails = await this.getFighter(fighter._id);
      const modalView = new ModalView(
        fighterDetails,
        this.setFighterHealth,
        this.setFighterAttack
      );
      FightersView.rootElement.append(modalView.element);
    } catch (error) {
      throw error;
    }
  }

  async getFighter(fighterId) {
    const fighter = await this.fightersDetailsMap.get(fighterId);
    if (fighter) {
      return fighter;
    } else {
      const fighterDetails = await fighterService.getFighterDetails(fighterId);
      return this.updateFighter(fighterDetails);
    }
  }

  async updateFighter(details) {
    const fighter = new Fighter(details);
    await this.fightersDetailsMap.set(details._id, fighter);
    return fighter;
  }

  async setFighterHealth(data) {
    const fighter = await this.getFighter(data._id);
    fighter.setHealth(data.health);
    return this.fightersDetailsMap.set(data._id, fighter);
  }

  async setFighterAttack(data) {
    const fighter = await this.getFighter(data._id);
    fighter.setAttack(data.attack);
    return this.fightersDetailsMap.set(data._id, fighter);
  }
}

export default FightersView;
