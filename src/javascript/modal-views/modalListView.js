import View from "../view";
import HealthView from "./healthView";
import AttackView from "./attackView";

class ModalListView extends View {
  constructor(fighter, setFighterHealth, setFighterAttack) {
    super();
    this.createModalList(fighter, setFighterHealth, setFighterAttack);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "li",
      className: "fighter-name"
    });
    nameElement.innerText = `Name: ${name}`;
    return nameElement;
  }

  createDefense(defense) {
    const defenseElement = this.createElement({
      tagName: "li",
      className: "fighter-defense"
    });
    defenseElement.innerText = `Defense: ${defense}`;
    return defenseElement;
  }

  createModalList(fighter, setFighterHealth, setFighterAttack) {
    const { name, health, attack, defense } = fighter;
    const nameElement = this.createName(name);
    const healthElement = new HealthView(fighter, setFighterHealth).element;
    const attackElement = new AttackView(fighter, setFighterAttack).element;
    const defenseElement = this.createDefense(defense);
    this.element = this.createElement({
      tagName: "ul",
      className: "modal__list"
    });
    this.element.append(
      nameElement,
      healthElement,
      attackElement,
      defenseElement
    );
  }
}

export default ModalListView;
