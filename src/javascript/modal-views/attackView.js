import View from "../view";

class AttackView extends View {
  constructor(fighter, setFighterAttack) {
    super();
    this.fighter = fighter;
    this.setFighterAttack = setFighterAttack;
    this.createAttack();
  }

  createAttack() {
    this.element = this.createElement({
      tagName: "li",
      className: "fighter-attack"
    });
    const text = this.createElement({ tagName: "span" });
    text.innerText = `Attack: ${this.fighter.attack}`;
    const btn = this.createEditBtn();
    this.element.append(text, btn);
  }

  createEditBtn() {
    const btn = this.createElement({ tagName: "button", className: "btn" });
    btn.innerText = "edit";
    btn.addEventListener("click", () => this.onEditClick(), false);
    return btn;
  }

  createSaveBtn() {
    const btn = this.createElement({
      tagName: "button",
      className: "btn"
    });
    btn.innerText = "save";
    btn.addEventListener("click", () => this.onSaveClick(), false);
    return btn;
  }

  onEditClick() {
    const input = this.createElement({
      tagName: "input",
      className: "attack-input"
    });
    const btn = this.createSaveBtn();
    this.element.innerHTML = "";
    this.element.append(input, btn);
  }

  async onSaveClick() {
    try {
      const val = document.querySelector(".attack-input").value;
      if (+val) {
        await this.setFighterAttack({ _id: this.fighter._id, attack: +val });

        const text = this.createElement({ tagName: "span" });
        text.innerText = `Attack: ${val}`;
        const btn = this.createEditBtn();
        this.element.innerHTML = "";
        this.element.append(text, btn);
      }
    } catch (error) {
      throw error;
    }
  }
}

export default AttackView;
