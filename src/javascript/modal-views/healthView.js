import View from "../view";

class HealthView extends View {
  constructor(fighter, setFighterHealth) {
    super();
    this.fighter = fighter;
    this.setFighterHealth = setFighterHealth;
    this.createHealth();
  }

  createHealth() {
    this.element = this.createElement({
      tagName: "li",
      className: "fighter-health"
    });
    const text = this.createElement({ tagName: "span" });
    text.innerText = `Health: ${this.fighter.health}`;
    const btn = this.createEditBtn();
    this.element.append(text, btn);
  }

  createEditBtn() {
    const btn = this.createElement({ tagName: "button", className: "btn" });
    btn.innerText = "edit";
    btn.addEventListener("click", () => this.onEditClick(), false);
    return btn;
  }

  createSaveBtn() {
    const btn = this.createElement({
      tagName: "button",
      className: "btn"
    });
    btn.innerText = "save";
    btn.addEventListener("click", () => this.onSaveClick(), false);
    return btn;
  }

  onEditClick() {
    const input = this.createElement({
      tagName: "input",
      className: "health-input"
    });
    const btn = this.createSaveBtn();
    this.element.innerHTML = "";
    this.element.append(input, btn);
  }

  async onSaveClick() {
    try {
      const val = document.querySelector(".health-input").value;
      if (+val) {
        await this.setFighterHealth({ _id: this.fighter._id, health: +val });

        const text = this.createElement({ tagName: "span" });
        text.innerText = `Health: ${val}`;
        const btn = this.createEditBtn();
        this.element.innerHTML = "";
        this.element.append(text, btn);
      }
    } catch (error) {
      throw error;
    }
  }
}

export default HealthView;
