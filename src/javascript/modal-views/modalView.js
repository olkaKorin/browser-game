import View from "../view";
import ModalListView from "./modalListView";

class ModalView extends View {
  constructor(fighter, setFighterHealth, setFighterAttack) {
    super();
    this.createModal(fighter, setFighterHealth, setFighterAttack);
  }

  createModal(fighter, setFighterHealth, setFighterAttack) {
    const listElement = new ModalListView(
      fighter,
      setFighterHealth,
      setFighterAttack
    ).element;
    const closeBtnElement = this.createCloseBtn();
    const titleElement = this.createModalTitle();
    this.element = this.createElement({ tagName: "div", className: "modal" });
    this.element.append(closeBtnElement, titleElement, listElement);
  }

  createCloseBtn() {
    const btn = this.createElement({
      tagName: "button",
      className: "modal__close-btn"
    });
    btn.innerText = "x";
    btn.addEventListener("click", event => this.element.remove(), false);
    return btn;
  }

  createModalTitle() {
    const title = this.createElement({
      tagName: "h2",
      className: "modal-title"
    });
    title.innerText = "Fighter's details";
    return title;
  }
}

export default ModalView;
